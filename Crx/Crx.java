import java.util.*;
import java.io.*;
import javax.sound.midi.*;
public class Crx{
	public static String b64(File f)throws Exception{
		byte[]bs=new byte[(int)f.length()];
		new FileInputStream(f).read(bs);
		return Base64.getEncoder().encodeToString(bs);
	}
	public static void pianoSound(PrintStream ps,HashMap<Integer,Integer>hs)throws Exception{
		ps.println("\t\thowler={};");
		for(int k:hs.keySet()){
			ps.print("\t\thowler["+k+"]=new Howl({src:['data:audio/mp3;base64,");
			File f=new File("G:\\Javascript\\Vpiano\\"+k+".mp3");
			ps.print(b64(f));
			ps.println("']});");
		}
	}
	public static void main(String[]args){
		HashMap<Integer,Integer>hs=new HashMap<>();
		int indiceTexto=0;
		long dt=0;
		try{
			String nom="Abre tu Corazón";
			Scanner sx=new Scanner(new File("crx.html"));
			Scanner ly=new Scanner(new File("G:\\Java\\Txtmid\\txtmid\\Abre tu Corazón_tenor.lyc"));
			PrintStream ps=new PrintStream(new FileOutputStream(new File(nom+"_Tenor.html")));
			while(sx.hasNextLine()){
				String l=sx.nextLine();
				if(l.equals("XXasdfasdfXX")){
					break;
				}
				ps.println(l);
			}
			Sequence s=MidiSystem.getSequence(new File("..\\"+nom+".mid"));
			int res=s.getResolution();
			ps.println("res="+res+";");
			HashMap<String,Long>h=new HashMap<String,Long>();
			Track[]ts=s.getTracks();
			int i=2;
			{
				Track t=ts[i];
				for(int j=0;j<t.size();j++){
					MidiEvent me=t.get(j);
					long tk=me.getTick();
					try{
						ShortMessage sm=(ShortMessage)me.getMessage();
						if(sm.getChannel()==9){
							continue;
						}
						if(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()>0){
							String k=sm.getData1()+":"+sm.getChannel();
							h.put(k,tk);
							hs.put(sm.getData1(),1);
						}
						if((sm.getCommand()==ShortMessage.NOTE_OFF)||(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()==0)){
							String k=sm.getData1()+":"+sm.getChannel();
							if(h.get(k)!=null){
								long tf=h.get(k);
								if(ly.hasNext()){
									ps.println("mx.add(new N("+(tf+dt)+","+(tk+dt)+","+sm.getData1()+","+sm.getChannel()+",'"+ly.next()+"'));");
								}else{
									ps.println("mx.add(new N("+(tf+dt)+","+(tk+dt)+","+sm.getData1()+","+sm.getChannel()+"));");
								}
							}
						}
					}catch(Exception e){
					}
				}
			}
			while(sx.hasNextLine()){
				String l=sx.nextLine();
				if(l.equals("howler={};")){
					pianoSound(ps,hs);
					continue;
				}
				ps.println(l);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}