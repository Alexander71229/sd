textos.push(new Texto(43200,75840,"Finished with my woman 'cause<br>She couldn't help me with my mind<br>People think I'm insane because<br>I am frowning all the time"));
textos.push(new Texto(104640,137280,"All day long I think of things<br>But nothing seems to satisfy<br>Think I'll lose my mind<br>If I don't find something to pacify"));
textos.push(new Texto(139200,164160,"Can you help me<br>Occupy my brain?<br>Oh yeah"));
textos.push(new Texto(196800,229440,"I need someone to show me<br>The things in life that I can't find<br>I can't see the things that make<br>True happiness, I must be blind"));
textos.push(new Texto(319680,352320,"Make a joke and I will sigh<br>And you will laugh and I will cry<br>Happiness I cannot feel<br>And love to me is so unreal"));
textos.push(new Texto(381120,413760,"And so as you hear these words<br>Telling you now of my state<br>I tell you to enjoy life<br>I wish I could but it's too late"));
