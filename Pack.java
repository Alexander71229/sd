import java.io.*;
import java.util.*;
public class Pack{
	public static String b64(File f)throws Exception{
		byte[]bs=new byte[(int)f.length()];
		new FileInputStream(f).read(bs);
		return Base64.getEncoder().encodeToString(bs);
	}
	public static void px(PrintStream ps,File f)throws Exception{
		try{
			Scanner s=new Scanner(f);
			while(s.hasNextLine()){
				ps.println(rx(s.nextLine(),".mp3"));
			}
		}catch(Exception e){c.U.imp(e);
		}
	}
	public static String bx(String l){
		return l.substring(l.indexOf("<script>")+14,l.indexOf(".js")+3);
	}
	public static String mp3(String r)throws Exception{
		File f=new File(r);
		byte[]bs=new byte[(int)f.length()];
		new FileInputStream(f).read(bs);
		return "data:audio/mp3;base64,"+Base64.getEncoder().encodeToString(bs);
	}
	public static int ix(String l,int p){
		int a=l.substring(0,p).lastIndexOf("'");
		int b=l.substring(0,p).lastIndexOf("\"");
		return Math.max(a,b);
	}
	public static String rx(String l,String z)throws Exception{
		int x=l.toLowerCase().indexOf(z.toLowerCase());
		if(x>=0){
			String s=l.substring(ix(l,x)+1,x+z.length());
			return l.replace(s,mp3(s));
		}
		return l;
	}
	public static void crearScript(PrintStream ps,String l)throws Exception{
		l=l.trim();
		ps.println("<script>");
		px(ps,new File(bx(l)));
		ps.println("</script>");
	}
	public static void pack(String nombre)throws Exception{
		PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+"_pkg.html")));
		Scanner s=new Scanner(new File(nombre+".html"));
		while(s.hasNextLine()){
			String l=s.nextLine();
			if(l.trim().startsWith("<script src")){
				crearScript(ps,l);
				continue;
			}
			ps.println(rx(l,".mp3"));
		}
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta="Log.txt";
			c.U.imp("Inicio");
			ArrayList<String>l=new ArrayList<>();
			l.add("enter-sandman");
			l.add("BreakingLaw");
			l.add("burning-times");
			l.add("paranoid");
			l.add("paradiseSongterr");
			l.add("i-died-for-you");
			l.add("For-Whom-The-Bell-Tolls");
			l.add("seek-destroy");
			l.add("Watching");
			l.add("creeping-death");
			for(int i=0;i<l.size();i++){
				pack(l.get(i));
			}
			c.U.imp("Fin");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}