import java.util.*;
import java.io.*;
import javax.sound.midi.*;
public class Sd{
	public static void main(String[]args){
		HashMap<String,ArrayList<String>>hashTexto=new HashMap<String,ArrayList<String>>();
		int indiceTexto=0;
		long dt=0;
		double fx=2.;
		try{
			String nom="nothing-else-matters01";
			Scanner texto=null;
			try{
				texto=new Scanner(new File(nom+".mid"));
				String pista="";
				while(texto.hasNext()){
					String c=texto.next();
					if(c.equals("txxxx")){
						pista=texto.next();
						hashTexto.put(pista,new ArrayList<String>());
					}else{
						hashTexto.get(pista).add(c);
					}
				}
			}catch(Throwable t){
				t.printStackTrace();
			}
			PrintStream ps=new PrintStream(new FileOutputStream(new File(nom+".js")));
			PrintStream ps2=new PrintStream(new FileOutputStream(new File(nom+".html")));
			Sequence s=MidiSystem.getSequence(new File(nom+".mid"));
			int res=s.getResolution();
			//dt=-res/2;
			ps.println("res="+res+";");
			HashMap<String,Long>h=new HashMap<String,Long>();
			Track[]ts=s.getTracks();
			for(int i=0;i<ts.length;i++){
				Track t=ts[i];
				for(int j=0;j<t.size();j++){
					MidiEvent me=t.get(j);
					long tk=Math.round(me.getTick()*fx);
					try{
						ShortMessage sm=(ShortMessage)me.getMessage();
						if(sm.getChannel()==9){
							continue;
						}
						if(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()>0){
							String k=sm.getData1()+":"+sm.getChannel();
							h.put(k,tk);
						}
						if((sm.getCommand()==ShortMessage.NOTE_OFF)||(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()==0)){
							String k=sm.getData1()+":"+sm.getChannel();
							if(h.get(k)!=null){
								long tf=h.get(k);
								ps.println("mx.add(new N("+(tf+dt)+","+(tk+dt)+","+sm.getData1()+","+sm.getChannel()+"));");
							}
						}
					}catch(Exception e){
					}
				}
			}
			ps2.println("<html>");
			ps2.println("\t<head>");
			ps2.println("\t\t<meta charset='UTF-8'>");
			ps2.println("\t</head>");
			ps2.println("\t<body style='touch-action:manipulation' onkeydown='boku(event,this)'>");
			ps2.println("\t\t<canvas onclick='bocu(event,this)'></canvas>");
			ps2.println("\t\t<input onblur='this.focus()' onclick='ocr(event,this)'/>");
			ps2.println("\t\t<script src='Pia.js'></script>");
			ps2.println("\t\t<script src='Sd.js'></script>");
			ps2.println("\t\t<script src='"+nom+".js'></script>");
			ps2.println("\t\t<script src='Sdm.js'></script>");
			ps2.println("\t\t<script src='texto.js'></script>");
			ps2.println("\t\t<script src='"+nom+"%extra.js'></script>");
			ps2.println("\t</body>");
			ps2.println("</html>");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}